#include<stdio.h>
//How to Remove/Delete value of existing Array
int main (){
    int n;
    scanf("%d",&n);     //Example: Size of Array 5
    int arr[n];       
    for(int i=0;i<n;i++)
    {
        scanf("%d",&arr[i]);
    }

    int pos;        
    scanf("%d",&pos);
    for(int i=pos;i<n-1;i++)
    {
        arr[i]=arr[i+1]; 
    }

    for(int i=0;i<n-1;i++)
    {
        printf("%d ",arr[i]);
    }
    return 0;
}