#include<stdio.h>
//How to insert a new value of existing Array
int main (){
    int n;
    scanf("%d",&n);     //Example: input 5
    int arr[n+1];       //Will be 6 cell generated
    for(int i=0;i<n;i++) //
    {
        scanf("%d",&arr[i]);//input(0-4) value
    }

    int pos,val;        
    scanf("%d %d",&pos,&val);   //Index should b considered; Input pos 1 & value 100
    for(int i=n;i>=pos+1;i--)   //Position will b 2(since index 1)
    {
        arr[i]=arr[i-1];    //let,i=5;=>arr[5]=arr[4]....arr[2]=arr[1]; when,i=1; loop will be stopped
    }

    //Checking the value how Interchange previous value each other into the existing array
    /*for(int i=0;i<=n;i++)
    {
        printf("%d ",arr[i]);
    }
    printf("\n");
    */

    arr[pos]=val; //Command for replacing value
    for(int i=0;i<=n;i++)
    {
        printf("%d ",arr[i]); //print the output after updating the index value.
    }
    return 0;
}