#include<stdio.h>
//Reverse Array : Two pointer Technique
int main (){
    int n;  //size of array
    scanf("%d",&n); 
    int arr[n]; //Declear an Array
    for(int i=0; i<n; i++)
    {
        scanf("%d ",&arr[i]); //Create an Array
    }
    int i=0, j=n-1;  //Since 'i' is always smaller than 'j'
    while (i<j) // If i>y or i==j; the work should stopped
    {       
        int tmp = arr[i]; //tmp variable for keeping value of 'i', so that no to loss
        arr[i]=arr[j];   //Inter-exchange the value to each other
        arr[j]=tmp; //then Assign the value of tmp into the array of 'j'
        i++;       //Increment loop for 'i'
        j--;      //Decrement loop for 'j'
    }
    for(int i=0; i<n; i++)      //print the output of Array
    {
        printf("%d ",arr[i]);
    }
    
    return 0;
}